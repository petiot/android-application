package br.com.petiot.petiot.DB

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import br.com.petiot.petiot.DB.DAO.DeviceDao
import br.com.petiot.petiot.DB.Entity.DeviceEntity

/**
 * Database schema that holds the list of Devices.
 */
@Database(
        entities = [DeviceEntity::class],
        version = 1,
        exportSchema = false
)
abstract class DeviceDatabase : RoomDatabase(){
    abstract fun deviceDao(): DeviceDao

    companion object {

        @Volatile
        private var INSTANCE: DeviceDatabase? = null

        fun getInstance(context: Context): DeviceDatabase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE
                            ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context.applicationContext,
                        DeviceDatabase::class.java, "Devices.db")
                        .build()
    }
}