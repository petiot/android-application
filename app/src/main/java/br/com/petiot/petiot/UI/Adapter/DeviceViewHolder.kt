package br.com.petiot.petiot.UI.Adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import br.com.petiot.petiot.DB.Entity.DeviceEntity
import br.com.petiot.petiot.R

class DeviceViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    private val animalName: TextView = view.findViewById(R.id.device_animal_name_textview)
    private val weight: TextView = view.findViewById(R.id.device_weight_textview)
    private val status: TextView = view.findViewById(R.id.device_status_textview)


    companion object {
        fun create(parent: ViewGroup): DeviceViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.device_view_item, parent, false)
            return DeviceViewHolder(view)
        }
    }

    private var device: DeviceEntity? = null

    init {
        view.setOnClickListener {
            device?.animalName?.let { animalName ->
                //TODO: Go to the page of the device when click on it.
                //val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                //view.context.startActivity(intent)
                Toast.makeText(view.context, "Clicou: ${animalName}", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun bind(device: DeviceEntity?) {
        if (device == null) {
            val resources = itemView.resources
            animalName.text = resources.getString(R.string.devices_loading)
            weight.visibility = View.GONE
            status.visibility = View.GONE
        } else {
            showDeviceData(device)
        }
    }

    private fun showDeviceData(device: DeviceEntity) {
        this.device = device
        animalName.text = device.animalName

        weight.text = device.weight.toString()
        status.text = device.deviceStatus.toString()
    }
}