package br.com.petiot.petiot.ViewModels

import android.app.Application
import android.arch.lifecycle.*
import android.arch.paging.PagedList
import br.com.petiot.petiot.DB.Entity.DeviceEntity
import br.com.petiot.petiot.DB.Entity.DeviceResult
import br.com.petiot.petiot.Data.DeviceRepository

class DeviceListViewModel (private val repository: DeviceRepository) : ViewModel() {

    companion object {
        private const val VISIBLE_THRESHOLD = 5
    }

    private val queryLiveData = MutableLiveData<String>()
    private val deviceResult: LiveData<DeviceResult> = Transformations.map(queryLiveData, {
        repository.get()
    })

    val devices: LiveData<PagedList<DeviceEntity>> = Transformations.switchMap(deviceResult,
            { it -> it.data })
    val networkErrors: LiveData<String> = Transformations.switchMap(deviceResult,
            { it -> it.networkErrors })

    /**
     * Get devices
     */
    fun getDevices() {
        //If there was a query, then it would go here.
        queryLiveData.postValue("android")
    }

    /*fun listScrolled(visibleItemCount: Int, lastVisibleItemPosition: Int, totalItemCount: Int) {
        if (visibleItemCount + lastVisibleItemPosition + VISIBLE_THRESHOLD >= totalItemCount) {
            val immutableQuery = lastQueryValue()
            if (immutableQuery != null) {
                repository.requestMore()
            }
        }
    }*/

    /**
     * Get the last query value.
     */
    fun lastQueryValue(): String? = queryLiveData.value

    /*private var observableDevices: MutableLiveData<List<DeviceEntity>> = MutableLiveData()

    constructor(application: Application) : super(application) {
        observableDevices.value = null
        loadDevices("")
    }

    fun getDevices (userId : String) : LiveData<List<DeviceEntity>>{
        if(observableDevices == null){
            observableDevices = MutableLiveData()
            loadDevices(userId)
        }
        return observableDevices
    }

    private fun loadDevices(userId : String) : LiveData<List<DeviceEntity>> {
        //TODO: Get the user's devices from the server! But for test i'm setting it manually
        var deviceList : MutableLiveData<List<DeviceEntity>> = MutableLiveData()
        var deviceListAux : MutableList<DeviceEntity> = mutableListOf()

        var deviceEntity1 = DeviceEntity("123", "Bob", 1.5, true)
        var deviceEntity2 = DeviceEntity("456", "Ralf", 2.0, true)
        var deviceEntity3 = DeviceEntity("789", "Mel", 0.5, false)


        deviceListAux.add(deviceEntity1)
        deviceListAux.add(deviceEntity2)
        deviceListAux.add(deviceEntity3)

        deviceList.value = deviceListAux
        //observableDevices.addSource(deviceList, observableDevices::setValue)
        observableDevices.value = deviceListAux
        return observableDevices
    }

    fun setTest(){
        //TODO: this code is copied from the method above, it must call the method above. REVIEW
        var deviceList : MutableLiveData<List<DeviceEntity>> = MutableLiveData()
        var deviceListAux : MutableList<DeviceEntity> = mutableListOf()

        var deviceEntity1 = DeviceEntity("123", "Teste1", 1.0, false)
        var deviceEntity2 = DeviceEntity("456", "Teste2", 2.5, true)
        var deviceEntity3 = DeviceEntity("789", "Teste3", 3.6, false)


        deviceListAux.add(deviceEntity1)
        deviceListAux.add(deviceEntity2)
        deviceListAux.add(deviceEntity3)

        deviceList.value = deviceListAux
        //observableDevices.addSource(deviceList, observableDevices::setValue)
        observableDevices.value = deviceListAux
    }*/
}