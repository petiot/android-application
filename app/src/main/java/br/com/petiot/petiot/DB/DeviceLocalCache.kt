package br.com.petiot.petiot.DB

import android.arch.lifecycle.LiveData
import android.arch.paging.DataSource
import android.util.Log
import br.com.petiot.petiot.DB.DAO.DeviceDao
import br.com.petiot.petiot.DB.Entity.DeviceEntity
import java.util.concurrent.Executor

class DeviceLocalCache(
        private val deviceDao: DeviceDao,
        private val ioExecutor: Executor
) {

    /**
     * Insert a list of Devices in the database, on a background thread.
     */
    fun insert(devices: List<DeviceEntity>, insertFinished: ()-> Unit) {
        ioExecutor.execute {
            Log.d("DeviceService", "inserting ${devices.size} devices")
            deviceDao.insert(devices)
            insertFinished()
        }
    }

    /**
     * Request a LiveData<PagedList<DeviceEntity>> from the Dao.
     */
    fun allDevices(): DataSource.Factory<Int, DeviceEntity> {
        return deviceDao.allDevices()
    }
}