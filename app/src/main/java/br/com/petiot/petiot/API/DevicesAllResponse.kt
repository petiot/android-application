package br.com.petiot.petiot.API

import android.arch.paging.PagedList
import br.com.petiot.petiot.DB.Entity.DeviceEntity
import com.google.gson.annotations.SerializedName

/**
 * Data class to hold Devices responses from getDevices API calls.
 */
data class DevicesAllResponse (
        @SerializedName("total_count") val total: Int = 0,
        @SerializedName("items") val items: List<DeviceEntity> = emptyList(),
        val nextPage: Int? = null
)