package br.com.petiot.petiot.DB.Entity

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList

data class DeviceResult(
        val data: LiveData<PagedList<DeviceEntity>>,
        val networkErrors: LiveData<String>
)