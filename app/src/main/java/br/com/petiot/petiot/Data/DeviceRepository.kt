package br.com.petiot.petiot.Data

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.LivePagedListBuilder
import android.util.Log
import br.com.petiot.petiot.API.DeviceService
import br.com.petiot.petiot.API.getDevices
import br.com.petiot.petiot.DB.DeviceLocalCache
import br.com.petiot.petiot.DB.Entity.DeviceResult

class DeviceRepository(
        private val service: DeviceService,
        private val cache: DeviceLocalCache
) {

    companion object {
        private const val DATABASE_PAGE_SIZE = 20
    }
    /**
     * Get devices
     */
    fun get(): DeviceResult {
        Log.d("DeviceRepository", "New query: get devices")
        //lastRequestedPage = 1
        //requestAndSaveData()

        // Construct the boundary callback
        val boundaryCallback = DevicesBoundaryCallback(service, cache)
        val networkErrors = boundaryCallback.networkErrors
        // Get data source factory from the local cache
        val dataSourceFactory = cache.allDevices()

        // Get the paged list
        val data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
                .setBoundaryCallback(boundaryCallback)
                .build()

        return DeviceResult(data, networkErrors)
    }
}