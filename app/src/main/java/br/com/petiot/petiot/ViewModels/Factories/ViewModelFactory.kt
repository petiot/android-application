package br.com.petiot.petiot.ViewModels.Factories

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import br.com.petiot.petiot.Data.DeviceRepository
import br.com.petiot.petiot.ViewModels.DeviceListViewModel


/**
 * Factory for ViewModels
 */
class ViewModelFactory(private val repository: DeviceRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DeviceListViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return DeviceListViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}