package br.com.petiot.petiot.UI

import android.app.Activity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import br.com.petiot.petiot.R
import kotlinx.android.synthetic.main.activity_main.*
//import kotlinx.android.synthetic.main.app_bar_main.*



//Firebase auth imports
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import java.util.Arrays.asList
import android.content.Intent
import android.media.Image
import android.net.Uri
import android.support.annotation.NonNull
import android.support.v4.widget.DrawerLayout
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toolbar
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.nav_header_main.*
import java.net.URI


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var mFirebaseAuth: FirebaseAuth? = null
    private var mAuthStateListener: FirebaseAuth.AuthStateListener? = null
    private val RC_SIGN_IN = 1

    //User Data
    private var mUserName: String? = null

    //For the Navigation between Fragments
    private lateinit var mainDrawer: DrawerLayout

    //Navigation drawer menu
    private lateinit var navigationMenuView : NavigationView
    private lateinit var navigationHeaderLayout : View
    private lateinit var userProfileImg : ImageView
    private lateinit var userDisplayNameText : TextView
    private lateinit var userEmailText : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        //Related to the navigation drawer menu
        navigationMenuView = findViewById(R.id.nav_view)
        navigationHeaderLayout = navigationMenuView.getHeaderView(0)
        userProfileImg = navigationHeaderLayout.findViewById(R.id.image_view_profile)
        userDisplayNameText = navigationHeaderLayout.findViewById(R.id.user_display_name_text)
        userEmailText = navigationHeaderLayout.findViewById(R.id.user_email_text)

        //Code related to the Navigation
        val fragmentHost = supportFragmentManager.findFragmentById(R.id.fragment_container) as NavHostFragment? ?: return
        val navigationController = fragmentHost.navController
        mainDrawer = findViewById(R.id.main_drawer_layout)

        mFirebaseAuth = FirebaseAuth.getInstance()
        mAuthStateListener = FirebaseAuth.AuthStateListener {firebaseAuth ->
            val user = firebaseAuth.currentUser
            if (user != null) {//User is logged in.
                NavigationUI.setupActionBarWithNavController(this, navigationController, mainDrawer)
                onSignedInInitialize(user)
            } else {
                //User is not logged in.
                startActivityForResult(
                        AuthUI.getInstance()
                                .createSignInIntentBuilder()
                                .setAvailableProviders(asList(
                                        AuthUI.IdpConfig.GoogleBuilder().build(),
                                        AuthUI.IdpConfig.FacebookBuilder().build(),
                                        AuthUI.IdpConfig.EmailBuilder().build()
                                ))
                                .build(),
                        RC_SIGN_IN)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            if (resultCode == Activity.RESULT_OK) {
                // Successfully signed in
                val user = FirebaseAuth.getInstance().currentUser
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // Sign in failed. If response is null the user canceled the sign-in flow using the back button.
                // Otherwise check response.getError().getErrorCode() and handle the error. The user could have exited the APP.
                finish()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(mainDrawer, Navigation.findNavController(this, R.id.fragment_container))
    }

    override fun onResume() {
        super.onResume()
        mFirebaseAuth!!.addAuthStateListener(mAuthStateListener!!)
    }

    override fun onPause() {
        super.onPause()
        if (mAuthStateListener != null) {
            mFirebaseAuth!!.removeAuthStateListener(mAuthStateListener!!)
        }
    }

    override fun onBackPressed() {
        if (main_drawer_layout.isDrawerOpen(GravityCompat.START)) {
            main_drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                // Handle the camera action
            }
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {
                signOut()
            }
        }

        main_drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun initializeNavigator(){
        //Floatting action button
        /*fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }*/

        val toggle = ActionBarDrawerToggle(
                this, main_drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        main_drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    private fun onSignedInInitialize(user: FirebaseUser) {
        userDisplayNameText.text = user.displayName
        userEmailText.text = user.email
        Picasso.get().load(user.photoUrl).into(userProfileImg)

        initializeNavigator()
        //It's not necessary to go to the initial screen because the Navigation component is handling it.
    }

    private fun onSignedOutCleanup() {
        mUserName = ""
    }

    private fun signOut() {
        AuthUI.getInstance().signOut(this)
    }
}
