package br.com.petiot.petiot.Data

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PagedList
import br.com.petiot.petiot.API.DeviceService
import br.com.petiot.petiot.API.getDevices
import br.com.petiot.petiot.DB.DeviceLocalCache
import br.com.petiot.petiot.DB.Entity.DeviceEntity

class DevicesBoundaryCallback(
        private val service: DeviceService,
        private val cache: DeviceLocalCache
) : PagedList.BoundaryCallback<DeviceEntity>() {

    companion object {
        private const val NETWORK_PAGE_SIZE = 50
    }

    // keep the last requested page. When the request is successful, increment the page number.
    private var lastRequestedPage = 1
    // LiveData of network errors.
    private val _networkErrors = MutableLiveData<String>()
    val networkErrors: LiveData<String>
        get() = _networkErrors
    // avoid triggering multiple requests in the same time
    private var isRequestInProgress = false

    override fun onZeroItemsLoaded() {
        requestAndSaveData()
    }

    override fun onItemAtEndLoaded(itemAtEnd: DeviceEntity) {
        requestAndSaveData()
    }

    private fun requestAndSaveData() {
        if (isRequestInProgress) return

        isRequestInProgress = true
        getDevices(service, "", lastRequestedPage, NETWORK_PAGE_SIZE, { devices ->
            cache.insert(devices, {
                lastRequestedPage++
                isRequestInProgress = false
            })
        }, { error ->
            _networkErrors.postValue(error)
            isRequestInProgress = false
        })
    }
}