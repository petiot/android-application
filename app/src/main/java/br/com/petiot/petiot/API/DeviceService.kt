package br.com.petiot.petiot.API

import android.arch.paging.PagedList
import android.util.Log
import br.com.petiot.petiot.DB.Entity.DeviceEntity
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private const val TAG = "DeviceService"
private const val IN_QUALIFIER = "in:name,description"


/**
 * Device API communication setup via Retrofit.
 */
interface DeviceService {
    /**
     * Get devices of the user
     */
    //TODO: Change this query to get devices from the user
    @GET("search/repositories?sort=stars")
    fun getDevices(@Query("q") query: String,
                   @Query("page") page: Int,
                   @Query("per_page") itemsPerPage: Int): Call<DevicesAllResponse>

    companion object {
        //TODO: Change to the base URL of our server
        private const val BASE_URL = "https://api.github.com/"

        fun create(): DeviceService {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
                    .addInterceptor(logger)
                    .build()
            return Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(DeviceService::class.java)
        }
    }
}


fun getDevices(
        service: DeviceService,
        query: String,
        page: Int,
        itemsPerPage: Int,
        onSuccess: (devices: List<DeviceEntity>) -> Unit,
        onError: (error: String) -> Unit) {
    Log.d(TAG, "query: $query, page: $page, itemsPerPage: $itemsPerPage")

    //TODO: remove the string "android"
    val apiQuery = query + "android" + IN_QUALIFIER

    service.getDevices(apiQuery, page, itemsPerPage).enqueue(
            object : Callback<DevicesAllResponse> {
                override fun onFailure(call: Call<DevicesAllResponse>?, t: Throwable) {
                    Log.d(TAG, "fail to get data")
                    onError(t.message ?: "unknown error")
                }

                override fun onResponse(
                        call: Call<DevicesAllResponse>?,
                        response: Response<DevicesAllResponse>
                ) {
                    Log.d(TAG, "got a response $response")
                    if (response.isSuccessful) {
                        val devices = response.body()?.items ?: emptyList()
                        onSuccess(devices)
                    } else {
                        onError(response.errorBody()?.string() ?: "Unknown error")
                    }
                }
            }
    )
}
