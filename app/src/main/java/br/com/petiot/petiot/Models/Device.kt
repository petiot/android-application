package br.com.petiot.petiot.Models

interface Device {
    fun getId() : String
    fun setId(id : String)
    fun getAnimalName() : String
    fun setAnimalName(animalName : String)
    fun getWeight() : Double
    fun setWeight(weight : Double)
    fun getStatus() : Boolean
    fun setStatus(status : Boolean)
}