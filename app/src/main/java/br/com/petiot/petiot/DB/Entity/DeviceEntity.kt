package br.com.petiot.petiot.DB.Entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "device")
data class DeviceEntity(
        @PrimaryKey @field:SerializedName("id") val id: Long,
        @field:SerializedName("name") val animalName: String,
        @field:SerializedName("full_name") val weight: String,
        @field:SerializedName("html_url") val deviceStatus: String
        /*@field:SerializedName("animalName") val animalName: String,
        @field:SerializedName("weight") val weight: Double,
        @field:SerializedName("deviceStatus") val deviceStatus: Boolean*/
)
/*class DeviceEntity() : Device {
    private var id : String = ""
    private var animalName : String = ""
    private var weight : Double = 0.0
    private var deviceStatus : Boolean = false


    constructor(id: String, animalName : String, weight : Double, deviceStatus : Boolean) : this() {
        setId(id)
        setAnimalName(animalName)
        setWeight(weight)
        setStatus(deviceStatus)
    }

    override fun getId(): String {
        return id
    }

    override fun setId(id: String) {
        this.id = id
    }

    override fun getAnimalName(): String {
        return animalName
    }

    override fun setAnimalName(animalName: String) {
        this.animalName = animalName
    }

    override fun getWeight(): Double {
        return weight
    }

    override fun setWeight(weight: Double) {
        this.weight = weight
    }

    override fun getStatus(): Boolean {
        return deviceStatus
    }

    override fun setStatus(status: Boolean) {
        this.deviceStatus = status
    }
}
*/

