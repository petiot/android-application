package br.com.petiot.petiot.UI.Adapter

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import br.com.petiot.petiot.DB.Entity.DeviceEntity

class DevicesAdapter : PagedListAdapter<DeviceEntity, RecyclerView.ViewHolder>(DEVICE_COMPARATOR) {

    companion object {
        private val DEVICE_COMPARATOR = object : DiffUtil.ItemCallback<DeviceEntity>() {
            override fun areItemsTheSame(oldItem: DeviceEntity, newItem: DeviceEntity): Boolean =
                    oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: DeviceEntity, newItem: DeviceEntity): Boolean =
                    oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return DeviceViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val repoItem = getItem(position)
        if (repoItem != null) {
            (holder as DeviceViewHolder).bind(repoItem)
        }
    }
}