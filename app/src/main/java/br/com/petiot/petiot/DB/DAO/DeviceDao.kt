package br.com.petiot.petiot.DB.DAO

import android.arch.lifecycle.LiveData
import android.arch.paging.DataSource
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import br.com.petiot.petiot.DB.Entity.DeviceEntity

@Dao
interface DeviceDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: List<DeviceEntity>)

    @Query("SELECT * FROM device")
    //fun allDevices(): LiveData<List<DeviceEntity>>
    fun allDevices() : DataSource.Factory<Int, DeviceEntity>
}