package br.com.petiot.petiot.UI

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.bluetooth.BluetoothClass
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.Navigation
import br.com.petiot.petiot.DB.Entity.DeviceEntity
import br.com.petiot.petiot.Dependencies.Injection

import br.com.petiot.petiot.R
import br.com.petiot.petiot.UI.Adapter.DevicesAdapter
import br.com.petiot.petiot.ViewModels.DeviceListViewModel
import com.google.android.gms.common.api.internal.LifecycleFragment
import org.jetbrains.annotations.Nullable

class InitialFragment : Fragment() {

    //private var deviceViewModel : DeviceListViewModel = null
    private lateinit var deviceViewModel  : DeviceListViewModel
    private val devicesAdapter = DevicesAdapter()

    private lateinit var deviceListRecyclerView : RecyclerView
    private lateinit var deviceListEmpty : TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_initial, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //TODO: put this code when the user click on a item in the list. Go to details.
        /*view.findViewById<Button>(R.id.teste_button)?.setOnClickListener{
            //The last Id is the ID of the relation between the Fragments on the nav_graph
            //Navigation.findNavController(it).navigate(R.id.action_initialFragment_to_deviceDetailFragment)
            deviceViewModel.setTest()
        }*/

        view.findViewById<FloatingActionButton>(R.id.floatingActionButton)?.setOnClickListener{
            //The last Id is the ID of the relation between the Fragments on the nav_graph
            Navigation.findNavController(it).navigate(R.id.action_initialFragment_to_deviceDetailFragment)
        }

        deviceListRecyclerView = view.findViewById(R.id.device_list_recyclerView)
        deviceListEmpty = view.findViewById(R.id.device_list_empty)

        //deviceViewModel = ViewModelProviders.of(this).get(DeviceListViewModel::class.java)
        deviceViewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(this.context!!))
                .get(DeviceListViewModel::class.java)

        // add dividers between RecyclerView's row items
        val decoration = DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL)
        deviceListRecyclerView.addItemDecoration(decoration)
        //setupScrollListener()

        initAdapter()
        deviceViewModel.getDevices()
        initGet()


        // var devices :
        //TODO: Get the logged in user Id
        //deviceViewModel.getDevices("123").observe(this,>
        /*val devicesObserver : Observer<List<DeviceEntity>>{
            fun onChanged(@Nullable newDevices : List<DeviceEntity>){
            }
        }*/

        //TODO: Get the logged in user Id
        /*deviceViewModel.getDevices("123").observe(this, Observer {
            var stringToShow = ""

            //TODO: This textview must be changed by a List element in the layout
            it!!.forEach {
                val idAux = it.getId()
                val nameAux = it.getAnimalName()
                val weightAux = it.getWeight()
                val statusAux = it.getStatus()

                stringToShow += idAux + " " + nameAux + " " + weightAux + " " + statusAux + " !!!\n"
            }

            val textView : TextView = view!!.findViewById(R.id.teste_textView)
            textView.text = stringToShow
        })*/
    }

    /*private fun setupScrollListener() {
        val layoutManager = deviceListRecyclerView.layoutManager as LinearLayoutManager
        deviceListRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = layoutManager.itemCount
                val visibleItemCount = layoutManager.childCount
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()

                deviceViewModel.listScrolled(visibleItemCount, lastVisibleItem, totalItemCount)
            }
        })
    }*/

    private fun initAdapter() {
        deviceListRecyclerView.adapter = devicesAdapter

        deviceViewModel.devices.observe(this, Observer<PagedList<DeviceEntity>> {
            Log.d("Activity", "list: ${it?.size}")
            showEmptyList(it?.size == 0)
            devicesAdapter.submitList(it)
        })

        deviceViewModel.networkErrors.observe(this, Observer<String> {
            Toast.makeText(this.context, "\uD83D\uDE28 Network Error: ${it}", Toast.LENGTH_LONG).show()
        })
    }

    private fun initGet() {
        //updateDevicesList()
    }

    private fun updateDevicesList() {
        deviceListRecyclerView.scrollToPosition(0)
        deviceViewModel.getDevices()
        devicesAdapter.submitList(null)
    }

    private fun showEmptyList(show: Boolean) {
        if (show) {
            deviceListEmpty.visibility = View.VISIBLE
            deviceListRecyclerView.visibility = View.GONE
        } else {
            deviceListEmpty.visibility = View.GONE
            deviceListRecyclerView.visibility = View.VISIBLE
        }
    }
}
