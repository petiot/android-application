package br.com.petiot.petiot.Dependencies

import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import br.com.petiot.petiot.API.DeviceService
import br.com.petiot.petiot.DB.DeviceDatabase
import br.com.petiot.petiot.DB.DeviceLocalCache
import br.com.petiot.petiot.Data.DeviceRepository
import br.com.petiot.petiot.ViewModels.Factories.ViewModelFactory
import java.util.concurrent.Executors

/**
 * Class that handles object creation.
 * Like this, objects can be passed as parameters in the constructors and then replaced for
 * testing, where needed.
 */
object Injection {
    /**
     * Creates an instance of [DeviceLocalCache] based on the database DAO.
     */
    private fun provideCache(context: Context): DeviceLocalCache {
        val database = DeviceDatabase.getInstance(context)
        return DeviceLocalCache(database.deviceDao(), Executors.newSingleThreadExecutor())
    }

    /**
     * Creates an instance of [DeviceRepository] based on the [DeviceService] and a
     * [DeviceLocalCache]
     */
    private fun provideDeviceRepository(context: Context): DeviceRepository {
        return DeviceRepository(DeviceService.create(), provideCache(context))
    }

    /**
     * Provides the [ViewModelProvider.Factory] that is then used to get a reference to
     * [ViewModel] objects.
     */
    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ViewModelFactory(provideDeviceRepository(context))
    }
}